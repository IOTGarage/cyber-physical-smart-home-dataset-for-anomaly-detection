# Cyber-Physical Smart Home Dataset for Anomaly Detection



## Intoduction

This dataset contains data from physical sensors, smart devices and network traffic. The dataset was captured in October/November 2022 at [Zero Bills (ZB House)](https://bregroup.com/ipark/parks/england/buildings/zero-bills-home/) in Building Reserach Establishment (BRE), Watford, UK. Code/ contains code files to convert data from RAW format to a syncronized time-line based dataset, samples of output are also given in relevant directories. To download the compete dataset visit [Dataset for Cyber-Physical Anomaly Detection in Smart Homes @ IEEE DataPort](https://ieee-dataport.org/documents/cyber-physical-anomaly-detection-smart-homes) or [Dataset for Cyber-Physical Anomaly Detection in Smart Homes @ Cardiff University Research Portal](https://doi.org/10.17035/d.2023.0259651425). Data report paper for this dataset is available at Frontiers on Internet of Things Journal [Dataset for Cyber-Physical Anomaly Detection in Smart Homes @ Frontiers in Internet of Things Journal](https://www.frontiersin.org/journals/the-internet-of-things).

## Folder Structure
- [Code](https://gitlab.com/IOTGarage/cyber-physical-smart-home-dataset-for-anomaly-detection/-/tree/main/Code) directory contains Jupyter Notebooks and Python Library for data processing.
- [Datasets/ZB-BRE](https://gitlab.com/IOTGarage/cyber-physical-smart-home-dataset-for-anomaly-detection/-/tree/main/Datasets/ZB-BRE) contains the following sub-directories
    - [Cyber](https://gitlab.com/IOTGarage/cyber-physical-smart-home-dataset-for-anomaly-detection/-/tree/main/Datasets/ZB-BRE/Cyber/) contains the following sub-directories
        - [PCAPFiles](https://gitlab.com/IOTGarage/cyber-physical-smart-home-dataset-for-anomaly-detection/-/tree/main/Datasets/ZB-BRE/Cyber/PCAPFiles) contains RAW format packet capture .pcap files seperated hour.
        - [CSVFiles](https://gitlab.com/IOTGarage/cyber-physical-smart-home-dataset-for-anomaly-detection/-/tree/main/Datasets/ZB-BRE/Cyber/CSVFiles) contains CSV files converted from .pcap files.
        - [Master](https://gitlab.com/IOTGarage/cyber-physical-smart-home-dataset-for-anomaly-detection/-/tree/main/Datasets/ZB-BRE/Cyber/Master) contains three different level of converted CSV files, joined as one file for all packets (sample only).
    - [Physical]()
        - [RAW](https://gitlab.com/IOTGarage/cyber-physical-smart-home-dataset-for-anomaly-detection/-/tree/main/Datasets/ZB-BRE/Physical/RAW) contains RAW format sensor data in CSV format.
        - [Master](https://gitlab.com/IOTGarage/cyber-physical-smart-home-dataset-for-anomaly-detection/-/tree/main/Datasets/ZB-BRE/Physical/Master) contains all devices sensor's data in transposed CSV format.
    - [CyPhy]()
        - [RAW](https://gitlab.com/IOTGarage/cyber-physical-smart-home-dataset-for-anomaly-detection/-/tree/main/Datasets/ZB-BRE/CyPhy/RAW) contains RAW format smart devices data from CU and BRE devices in CSV format.
        - [Master](https://gitlab.com/IOTGarage/cyber-physical-smart-home-dataset-for-anomaly-detection/-/tree/main/Datasets/ZB-BRE/CyPhy/Master) contains all CU and BRE devices data in transposed CSV format (smaple only).
    - [Master](https://gitlab.com/IOTGarage/cyber-physical-smart-home-dataset-for-anomaly-detection/-/tree/main/Datasets/ZB-BRE/Master) contains integrated dataset from multiple sources (sample only).˝
    - [Misc](https://gitlab.com/IOTGarage/cyber-physical-smart-home-dataset-for-anomaly-detection/-/tree/main/Datasets/ZB-BRE/Misc) contains files to understand and explore datasets and code.