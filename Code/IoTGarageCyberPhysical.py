#Required Libraries
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
import pandas as pd
from datetime import datetime
import numpy as np
from matplotlib import pyplot as plt
import time
from scipy import stats
import plotly.express as px
import plotly.graph_objects as go
# from google.colab import drive
import json
import pickle
from sklearn import svm
import os
import tqdm

__version__=0.01
def PlotPhyData (DataFrame, List, TR, Title=""):
  colors = px.colors.qualitative.Plotly
  fig = go.Figure()
  i=0
  ST = TR[0]
  ET = TR[1]
  TimeFrameRows = (DataFrame['Timestamp'] > ST) & (DataFrame['Timestamp'] <= ET)
  DataFrame = DataFrame.loc[TimeFrameRows]
  fig = px.line(DataFrame, x='Timestamp')
  # Only thing I figured is - I could do this 
  for a in List[1:]:
    fig.add_scatter(x=DataFrame['Timestamp'], y=DataFrame[a], name=a, mode='lines')
# Show plot 
# fig.show()
#   fig.update_layout(
#     title=Title,
#     xaxis_title="Time",
#     yaxis_title="Value",
#     legend_title="Sensor",
#     font=dict(
#         family="Courier New, monospace",
#         size=18,
#         color="RebeccaPurple"
#     ))
  fig.update_yaxes(range = [0,300])
  fig.update_layout(
     title="(ActivityID) - (Description) Between (Time In) to (Time Out)",
     xaxis_title="Time",
     yaxis_title="Sensing Level",
     legend_title="Sensor Name",
    font=dict(
        family="Courier New, monospace",
        size=18,
        color="RebeccaPurple"
    ))
  fig.show()
    
#   for H in List:
#     fig.add_traces(go.Scatter(x=DataFrame['Timestamp'], y = DataFrame[H], mode = 'lines', name=H, line=dict(color=colors[i])))
#     if (i==9):
#       i=-1
#     i+=1
  
# Extract Phy Headers from Headers File
def PhyHeadersExtract(HeadersFile):
    #Header for each type of device, it will help to get the specific device's data
    Headers = open(BaseFolder+HeadersFile, "r")
    Headers = Headers.read()
    Headers = Headers.split(",")
    Hs = []
    for H in Headers:
      Hs.append(H.replace("\"", ""))
    for i in range(len(Hs)):
        Hs[i]=Hs[i].lstrip(" ")
    return Hs
def SaveToHTML (DataFrame, List, TR, Title=""):
  colors = px.colors.qualitative.Plotly
  fig = go.Figure()
  i=0
  ST = TR[0]
  ET = TR[1]
  TimeFrameRows = (DataFrame['Timestamp'] > ST) & (DataFrame['Timestamp'] <= ET)
  DataFrame = DataFrame.loc[TimeFrameRows]
  for H in List:
    fig.add_traces(go.Scatter(x=DataFrame['Timestamp'], y = DataFrame[H], mode = 'lines', name=H, line=dict(color=colors[i])))
    if (i==9):
      i=-1
    i+=1
  fig.update_layout(
    title=Title,
    xaxis_title="Time",
    yaxis_title="Value",
    legend_title="Sensor",
    font=dict(
        family="Courier New, monospace",
        size=18,
        color="RebeccaPurple"
    ))
  fig.write_html(Title+".html")
def GetActivityData(StartTime, EndTime, DataFrame):
  ST=pd.to_datetime(StartTime)
  ET=pd.to_datetime(EndTime)
  ActivityRows = (DataFrame['Timestamp'] > StartTime) & (DataFrame['Timestamp'] <= EndTime)
  return DataFrame.loc[ActivityRows]
def ReadActivities(FileLocation):
  tempfile = open(FileLocation, "r")
  FileContents = tempfile.readlines()
  ActLog = []  
  for a in FileContents[1:]:
    a = a.split(",")
    a[2]=a[2].replace("\n", "")
    a[2]=str(a[2])
    ActLog.append(a)
  return ActLog
def AddDST (DataFrame):
#This Function (AddDST) will add one hour in timestamp due to Daylight Saving, the function needs to be updated after till 31st October 2022
  DataFramePreDLS = DataFrame[DataFrame["Timestamp"]<"2022-10-31"]
  DataFramePostDLS = DataFrame[DataFrame["Timestamp"]>"2022-10-31"]
  DataFramePreDLS["Timestamp"] = DataFramePreDLS["Timestamp"] + pd.Timedelta(hours=1)
  DataFrame = pd.concat([DataFramePreDLS, DataFramePostDLS])
  return DataFrame
def HostToIP(NetworkID,HostID):
  return NetworkID+"."+str(HostID)
def GetLabelsFromFile(FileLocation):
  tempfile = open(FileLocation, "r")
  FileContents = tempfile.read()
  # print (FileContents)
  # FileContents = FileContents.replace("\'", "\"")
  Labels = json.loads(FileContents)
  tempfile.close()
  return Labels
def GetDBFileList(Location, FilePrefix):
  f = []
  for (dirpath, dirnames, filenames) in os.walk(Location):
      f.extend(filenames)
      break
  f = [v for v in f if FilePrefix in v]
  return  [Location+"/" + x for x in f if isinstance(x, str)]
def SensorValueAt(DataFrame, Sensor, Timestamp):
    return DataFrame[Sensor][DataFrame["Timestamp"]>Timestamp].iloc[0]
# drive.mount('/content/drive')
#Raw Datasets
GoogleDrive = "/content/drive/MyDrive/Datasets/"
LocalDrive = "/home/dcp/CyberASAP/Datasets/"
BaseFolder=LocalDrive+"ZB-BRE/"
#Get Activity Labels from Json file
# ActivityLabels = GetLabelsFromFile(BaseFolder+"ActivityLabels.json")
# Get device details
PhysicalDevices = {1:"Enterance Door", 2: "Kitchen Cabinet", 3:"Living Room", 4:"Bathroom", 5:"Bedroom"}
Cameras = {"10.11.12.200":"Kitchen Area"}
BRE_CyPhyDevices = GetLabelsFromFile (BaseFolder+"Misc/BRE_CyPhyDevices.json")
CU_CyPhyDevices = GetLabelsFromFile (BaseFolder+"Misc/CU_CyPhyDevices.json")
CyberDevices = GetLabelsFromFile (BaseFolder+"Misc/CyberDevices.json")
# CU_ReflectiveDevices = GetLabelsFromFile (BaseFolder+"CU_ReflectiveDevices.csv")
# BRE_ReflectiveDevices = GetLabelsFromFile (BaseFolder+"BRE_ReflectiveDevices.csv")
# TriggerDevices = GetLabelsFromFile (BaseFolder+"TriggerDevices.csv")
# Create a list of Database files with categories
DatabaseFiles = {}
DatabaseFiles["Physical"]=[]
DatabaseFiles["BRE_CyPhy"]=[]
DatabaseFiles["CU_CyPhy"]=[]
DatabaseFiles["Cyber"]=[]
DatabaseFiles["Sniffer"]=[]
DatabaseFiles["Physical"]=GetDBFileList(BaseFolder+"Physical/RAW", "SensorData")
DatabaseFiles["BRE_CyPhy"]=GetDBFileList(BaseFolder+"CyPhy/RAW", "BRECyPhy")
DatabaseFiles["CU_CyPhy"]=GetDBFileList(BaseFolder+"CyPhy/RAW", "CUCyPhy")
DatabaseFiles["Cyber"]=GetDBFileList(BaseFolder+"Cyber/PCAPFiles", "PacketCaptures")
DatabaseFiles["Sniffer"]=GetDBFileList(BaseFolder+"Cyber", "Sniffer")
DatabaseFiles["Activities"]=GetDBFileList(BaseFolder, "ACTLog")
# # Load Raw Physical Datasets into Dataframes
# # Physical Dataset convert into device based and store as individual files
PhyHeaders = PhyHeadersExtract("Misc/headers_BRE-ZB.csv")
# office_status_codes = {'open':1, 'closed':0}
# day_codes = {'monday':0,'tuesday':1,'wednesday':2,'thursday':3,'friday':4,'saturday':5,'sunday':6}
# Activities = ReadActivities(DatabaseFiles["Activities"][0])
# ActDF = pd.DataFrame (Activities, columns = ['StartTime', 'EndTime', 'ActID'])

CU_CyPhy_Headers = ["state_id","entity_id","state","attributes","event_id","last_changed","Timestamp","old_state_id","attributes_id","context_id","context_user_id","context_parent_id","origin_idx"]
CUCP_NR_Labels=["state_id","attributes","event_id","d", "old_state_id","attributes_id","context_id","context_user_id","context_parent_id","origin_idx"]
print ("IoTGarage Cyber Physical Python Library version %s Import Successful", __version__)
# DatabaseFiles
